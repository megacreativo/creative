<?php

class indexController extends Controller {
	function __construct() {		
		parent::__construct(__CLASS__);
		$this->controller = __CLASS__;
		$this->module = str_ireplace('controller','', $this->controller);		
	}
	
	/**
	* Utilizado para el Metodo GET
	* 
	* @return
	*/
	public function index(){
        $response = array(
            'status'=>200, 
            'statusText'=>'Ok', 
            'version'=>'1',
			'time'=> Creative::get( 'Session' )->get('CRERATIVE_TIME')
        );			
        http_response_code(200);
        header('Content-Type: application/json; charset=utf8');
        echo json_encode($response, JSON_PRETTY_PRINT);
        exit;
	}
}

?>