<?php


class usersController extends Controller {
	
	private $_filters = array( 
		'Todos'		=> 'all',
		'Nombre'	=> 'name',
		'Módulo de inicio'	=> 'default_module',
	);
	
	
	function __construct() {		
		parent::__construct(__CLASS__);
		$this->controller = __CLASS__;
		$this->module = str_ireplace('controller','', $this->controller);		
		$this->model_module = $this->load_model($this->module);
		$this->model_meta = $this->load_model($this->module .'_meta');
		$this->model_meta->change_pk('user_id');
	}
	
	/**
	* Utilizado para el Metodo GET
	* 
	* @return
	*/
	public function index(){		
		$this->basic_index(__FUNCTION__, func_get_args());
	}
	
	
	/**
	* 
	* 
	* @return
	*/
	public function find( $id = NULL ){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		$this->model_module->id = $id;
		$data = $this->model_module->find( array(
			"user_decp" =>
				"CONCAT(name, ' ', last_name)",
			"status_text" =>
				"CASE 
					WHEN status = 0 THEN 'Inactivo' 
					WHEN status = 1 THEN 'Activo' 
				END",
			"status_class" =>
				"CASE 
					WHEN status = 0 THEN 'danger' 
					WHEN status = 1 THEN 'success' 
				END",
			"status_info" => 
				"CASE 
					WHEN status = 0 THEN 'Inactivo' 
					WHEN status = 1 THEN 'Activo' 
				END",
			)
		);
		
		
		
		$this->model_meta->user_id = $id;
		$permissions = $this->model_meta->search();
		$data['permissions'] = $permissions;
		
		
		$this->view->response(200, 
			array(
				'method'	=> 'GET',
				'count' 	=> count($data),
				'count_permissions' 	=> count($permissions),
				'data'		=> $data,
				
			)
		);
		
		
	}
	


	/**
	* 
	* 
	* @return
	*/
	public function search( $filter, $value){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		if( !array_search( $filter, $this->_filters ) ){
			$this->view->response(422, 
				array(
					'method'	=> 'GET',
					'data'		=> array(),
					'count' 	=> 0
				)
			);
		}
		
		if( $filter == 'all' ){
			$values = array();
			
			foreach( $this->_filters as $key => $val){
				if( $val !== 'all' ){
					$filters[$val] = "%".$value."%";
				}
			}
			
			$data = $this->model_module->likeor($filters, array(
				"user_decp" =>
					"CONCAT(name, ' ', last_name)",
				"status_text" =>
					"CASE 
						WHEN status = 0 THEN 'Inactivo' 
						WHEN status = 1 THEN 'Activo' 
					END",
				"status_class" =>
					"CASE 
						WHEN status = 0 THEN 'danger' 
						WHEN status = 1 THEN 'success' 
					END",
				"status_info" => 
					"CASE 
						WHEN status = 0 THEN 'Inactivo' 
						WHEN status = 1 THEN 'Activo' 
					END",
				)
			);
			
		} else {
			
			$data = $this->model_module->likeor( array($filter=>$value), array(
				"user_decp" =>
					"CONCAT(name, ' ', last_name)",
				"status_text" =>
					"CASE 
						WHEN status = 0 THEN 'Inactivo' 
						WHEN status = 1 THEN 'Activo' 
					END",
				"status_class" =>
					"CASE 
						WHEN status = 0 THEN 'danger' 
						WHEN status = 1 THEN 'success' 
					END",
				"status_info" => 
					"CASE 
						WHEN status = 0 THEN 'Inactivo' 
						WHEN status = 1 THEN 'Activo' 
					END",
				)
			);
		}
		
		
		
		$this->view->response(200, 
			array(
				'method'	=> 'GET',
				'data'		=> $data,
				'count' 	=> count($data)
			)
		);
	}
	
	
	
	/**
	* Guardar un nuevo registro
	* 
	* @return
	*/
	public function post( ){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		//Recoger valor de variables
		$email 			= $this->get_string('email');
		$pass 			= $this->get_string('pass');
		$profiles_id 	= $this->get_string('profiles_id');
		$name 			= $this->get_string('name');
		$last_name 		= $this->get_string('last_name');
		$nicname 		= $this->get_string('nicname');
		$status 		= $this->get_int('status');
		
		$permissions 	= $this->get_post('permissions');
		$access_field 	= $this->get_post('access_field');
		
		
		if( strlen($name) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>Nombre</strong> es requerido para continuar. Debe tener entre 3 y 25 caracteres.',
					'field' => 'name',
					'data' => $_POST
				)
			);
		}
		
		if( strlen($email) < 5 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>E-mail</strong> es requerido para continuar.',
					'field' => 'name',
					'data' => $_POST
				)
			);
		} 
		
		$check_pass = $this->check_pass($pass);
		if( $check_pass['status'] < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=> $check_pass['statusText'],
					'field' => 'pass1',
					'data' => $_POST
				)
			);
		}
		
		if( $status < 0 OR $status > 1 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Seleccione el Estatus del registro para continuar.',
					'field' => 'status',
					'data' => $_POST
				)
			);
		} 
		
		
		$exists = $this->model_module->exists($email, 'email');
		if( $exists == TRUE ){
			$this->view->response(422, 
				array(
					'statusText'=>'Existe un registro con el mismo <strong>E-mail</strong>.',
					'field' => 'email',
					'data' => $_POST
				)
			);
		}
		
		
		$this->model_module->begin();
		
		$pass = $this->encode($pass, HASH_KEY);
		
		//Asignación de valores y Guardado
		$this->model_module->email 			= $this->pSQL($email);
		$this->model_module->pass 			= $pass;
		$this->model_module->hash			= HASH_KEY;
		$this->model_module->profile_id		= $this->pSQL($profiles_id);
		$this->model_module->name			= $this->pSQL($name);
		$this->model_module->last_name		= $this->pSQL($last_name);
		$this->model_module->nicname		= $this->pSQL($nicname);
		$this->model_module->status	 		= $status;
				
		$this->model_module->create();
		$id = $this->model_module->last_id();
		
		$this->model_meta->change_pk('user_id');
		$this->model_meta->delete($id);
		
		foreach( $permissions as $modulo => $modulo_attr ){
			$this->model_meta->user_id	= $id;
			$this->model_meta->name 	= $this->pSQL($modulo);
			$this->model_meta->content	= implode(',',$modulo_attr);
			$this->model_meta->attr		= 'permission-module';
			
			$this->model_meta->create();
		}
		
		foreach( $access_field as $modulo => $modulo_access ){
			$access = '';
			foreach( $modulo_access as $key => $value ){
				$access .= $key.':'.$value['access'].',';
			}
			$access = substr($access, 0, strlen($access)-1 );
			$this->model_meta->user_id	= $id;
			$this->model_meta->name 	= $this->pSQL($modulo);
			$this->model_meta->content	= $access;
			$this->model_meta->attr		= 'permission-field';
			
			$this->model_meta->create();
		}
		
		
		$this->model_module->commit();
		
		
		//Obtener el registro actual
		
		$this->model_module->id = $id;
		$data = $this->model_module->find( array(
			"user_decp" =>
				"CONCAT(name, ' ', last_name)",
			"status_text" =>
				"CASE 
					WHEN status = 0 THEN 'Inactiva' 
					WHEN status = 1 THEN 'Activa' 
				END",
			"status_class" =>
				"CASE 
					WHEN status = 0 THEN 'danger' 
					WHEN status = 1 THEN 'success' 
				END",
			"status_info" => 
				"CASE 
					WHEN status = 0 THEN 'Carrera inactiva' 
					WHEN status = 1 THEN 'Carrera activa' 
				END",
			)
		);
		
		$data['pass'] = '';
		$data['activation_key'] = '';
		$data['ambit'] = '';
		$data['hash'] = '';
		$data['reset_pass_key'] = '';
		
		
		$this->model_meta->user_id = $id;
		$permissions = $this->model_meta->search();
		$data['permissions'] = $permissions;
		
		//Devolver la respuesta
		$this->view->response(201, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido guardado',
			'icon' => 'success',
			'data' => $data
		));
	}
	
	/**
	* 
	* 320 - 200
	* 500 - 500 
	* 1   - 650
	* @return
	*/
	public function put(){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		//Recoger valor de variables
		$id 			= $this->get_put('id');
		$email 			= $this->get_put('email');
		$pass 			= $this->get_put('pass');
		$profile_id 	= $this->get_put('profile_id');
		$name 			= $this->get_put('name');
		$last_name 		= $this->get_put('last_name');
		$nicname 		= $this->get_put('nicname');
		$status 		= $this->get_put('status');
		
		$permissions 	= $this->get_put('permissions');
		$access_field 	= $this->get_put('access_field');
		
		
		if( strlen($name) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>Nombre</strong> es requerido para continuar. Debe tener entre 3 y 25 caracteres.',
					'field' => 'name',
					'data' => $_POST
				)
			);
		}
		
		if( strlen($email) < 5 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>E-mail</strong> es requerido para continuar.',
					'field' => 'name',
					'data' => $_POST
				)
			);
		} 
		
		if( $pass != '' ){
			$check_pass = $this->check_pass($pass);
			if( $check_pass['status'] < 3 ){
				$this->view->response(422, 
					array(
						'statusText'=> $check_pass['statusText'],
						'field' => 'pass1',
						'data' => $_POST
					)
				);
			}			
		}

		
		if( $status < 0 OR $status > 1 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Seleccione el Estatus del registro para continuar.',
					'field' => 'status',
					'data' => $_POST
				)
			);
		} 
		
		$this->model_module->begin();
		
		$pass = $this->encode($pass, HASH_KEY);
		
		//Asignación de valores y Guardado
		if( $pass != '' ) $this->model_module->pass = $pass;
		
		$this->model_module->email 			= $this->pSQL($email);
		$this->model_module->hash			= HASH_KEY;
		$this->model_module->profile_id		= $this->pSQL($profile_id);
		$this->model_module->name			= $this->pSQL($name);
		$this->model_module->last_name		= $this->pSQL($last_name);
		$this->model_module->nicname		= $this->pSQL($nicname);
		$this->model_module->status	 		= $status;
				
		$this->model_module->update( $id );
		
		//Obtener el ID asigando desde la Base de Datos
		$affected = $this->model_module->affected();
		
		$this->model_meta->change_pk('user_id');
		$this->model_meta->delete($id);
		
		foreach( $permissions as $modulo => $modulo_attr ){
			$this->model_meta->user_id	= $id;
			$this->model_meta->name 	= $this->pSQL($modulo);
			$this->model_meta->content	= implode(',',$modulo_attr);
			$this->model_meta->attr		= 'permission-module';
			
			$this->model_meta->create();
		}
		
		foreach( $access_field as $modulo => $modulo_access ){
			$access = '';
			foreach( $modulo_access as $key => $value ){
				$access .= $key.':'.$value['access'].',';
			}
			$access = substr($access, 0, strlen($access)-1 );
			$this->model_meta->user_id	= $id;
			$this->model_meta->name 	= $this->pSQL($modulo);
			$this->model_meta->content	= $access;
			$this->model_meta->attr		= 'permission-field';
			
			$this->model_meta->create();
		}
		
		
		$this->model_module->commit();
		
		
		//Obtener el registro actual
		
		$this->model_module->id = $id;
		$data = $this->model_module->find( array(
			"user_decp" =>
				"CONCAT(name, ' ', last_name)",
			"status_text" =>
				"CASE 
					WHEN status = 0 THEN 'Inactiva' 
					WHEN status = 1 THEN 'Activa' 
				END",
			"status_class" =>
				"CASE 
					WHEN status = 0 THEN 'danger' 
					WHEN status = 1 THEN 'success' 
				END",
			"status_info" => 
				"CASE 
					WHEN status = 0 THEN 'Carrera inactiva' 
					WHEN status = 1 THEN 'Carrera activa' 
				END",
			)
		);
		
		$data['pass'] = '';
		$data['activation_key'] = '';
		$data['ambit'] = '';
		$data['hash'] = '';
		$data['reset_pass_key'] = '';
		
		
		$this->model_meta->user_id = $id;
		$permissions = $this->model_meta->search();
		$data['permissions'] = $permissions;
		
		//Devolver la respuesta
		$this->view->response(201, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido guardado',
			'icon' => 'success',
			'data' => $data
		));		
	
	}
	
	/**
	* 
	* 
	* @return
	*/
	public function delete( $id = NULL ){
		
		if( strlen($id) < 1 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Se debe especificar un ID válido para continuar con la operación',
					'icon'=>'warning',
					'data' => array('id'=>$id)
				)
			);
		} 
		
		
		$this->model_module->delete($id);
		
		//Devolver la respuesta
		$this->view->response(204, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido eliminado',
			'icon' => 'success',
			'data' => array('id'=>$id)
		));
	}
	
}
?>