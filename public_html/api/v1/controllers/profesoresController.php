<?php

require_once 'usertype.php';

class profesoresController extends Controller {
	
	private $_filters = array( 
		'Todos'		=> 'all',
		'Cedula'	=> 'cedula',
		'Nombre'	=> 'profesor_nombre',
		'Sede'		=> 'sede_id',
		'Estado'	=> 'estado',
		'Ciudad'	=> 'ciudad',
		'Parroquia'	=> 'parroquia',
	);
	
	
	function __construct() {		
		parent::__construct(__CLASS__);
		
		$this->controller = __CLASS__;
		$this->module = str_ireplace('controller','', $this->controller);		
		$this->model_module = $this->load_model($this->module);
		$this->model_module_view = $this->load_model('view_' . $this->module);
		$this->model_users = $this->load_model('users');
	}
	
	/**
	* Utilizado para el Metodo GET
	* 
	* @return
	*/
	public function index(){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		$data = $this->model_module_view->all();
		
		$this->view->response(200, 
			array(
				'method'	=> 'GET',
				'data'		=> $data,
				'count' 	=> count($data)
			)
		);
	}
	
	
	/**
	* 
	* 
	* @return
	*/
	public function find( $id = NULL ){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		$this->model_module_view->id = $id;
		$data = $this->model_module_view->find();
		$this->view->response(200, 
			array(
				'method'	=> 'GET',
				'data'		=> $data,
				'count' 	=> count($data)
			)
		);
	}
	


	/**
	* 
	* 
	* @return
	*/
	public function search( $filter, $value){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		if( !array_search( $filter, $this->_filters ) ){
			$this->view->response(422, 
				array(
					'method'	=> 'GET',
					'data'		=> array(),
					'count' 	=> 0
				)
			);
		}
		
		if( $filter == 'all' ){
			$values = array();
			
			foreach( $this->_filters as $key => $val){
				if( $val !== 'all' ){
					$filters[$val] = "%".$value."%";
				}
			}
			
			$data = $this->model_module_view->likeor($filters);
			
		} else {
			
			$data = $this->model_module_view->likeor( array($filter=>$value));
		}
		
		
		
		$this->view->response(200, 
			array(
				'method'	=> 'GET',
				'data'		=> $data,
				'count' 	=> count($data)
			)
		);
	}
	
	
	
	/**
	* Guardar un nuevo registro
	* 
	* @return
	*/
	public function post( ){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		//Recoger valor de variables
		$name 		= $this->get_string('name');
		$last_name 	= $this->get_string('last_name');
		$email 		= $this->get_string('email');
		
		$cedula 	= $this->get_string('cedula');
		$tel_movil 	= $this->get_string('tel_movil');
		$tel_habitacion 	= $this->get_string('tel_habitacion');
		$tel_contacto 		= $this->get_string('tel_contacto');
		$nivel_profesion 	= $this->get_string('nivel_profesion');
		$area_profesional 	= $this->get_string('area_profesional');
		$estado 	= $this->get_string('estado');
		$ciudad 	= $this->get_string('ciudad');
		$parroquia 	= $this->get_string('parroquia');
		$direccion	= $this->get_string('direccion');
		$sede_id 	= $this->get_string('sede_id');
		$status 	= $this->get_int('status');
						
		//Validación de campos
		if( strlen($cedula) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'La <strong>Cédula</strong> es requerida para continuar.',
					'field' => 'codigo',
					'icon'=>'warning',
					'data' => $_POST
				)
			);
		} 
		
		if( strlen($name) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>Nombre</strong> es requerido para continuar. Debe tener entre 3 y 25 caracteres.',
					'field' => 'name',
					'data' => $_POST
				)
			);
		} 
		
		if( strlen($last_name) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>Apellido</strong> es requerido para continuar. Debe tener entre 3 y 25 caracteres.',
					'field' => 'last_name',
					'data' => $_POST
				)
			);
		} 
		
		if( $status < 0 OR $status > 1 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Seleccione el Estatus del registro para continuar.',
					'field' => 'status',
					'data' => $_POST
				)
			);
		} 
		
		if( $sede_id <0 ){
			$sede_id = NULL;
		}
		
		//Validar existencia previa del registro		
		if( $email != '' ){
			//Validar existencia previa del registro
			$exists = $this->model_users->exists($email, 'email');
			if( $exists == TRUE ){
				$this->view->response(422, 
					array(
						'statusText'=>'Existe un registro con el mismo <strong>E-mail</strong> ingresado.',
						'field' => 'email',
						'data' => $_POST
					)
				);
			}
	
		}
		
		$exists = $this->model_module->exists($cedula, 'cedula');
		if( $exists == TRUE ){
			$this->view->response(422, 
				array(
					'statusText'=>'Existe un registro con la misma <strong>Cédula de Identdidad</strong> ingresada.',
					'field' => 'cedula',
					'data' => $_POST
				)
			);
		}
		
		$this->model_module->begin();
		
		//Crear usuario en la tabla de usuario
		$this->model_users->profiles_id = NULL;
		$this->model_users->name 		= $this->pSQL($name);
		$this->model_users->last_name 	= $this->pSQL($last_name);
		$this->model_users->user_type 	= UserType::PROFESOR;
		$this->model_users->ambit	 	= BACKEND;
		
		$this->model_users->create();
		$user_id = $this->model_users->last_id();
		
		$area_profesional = implode(',',$area_profesional);
		
		//Asignación de valores y Guardado
		$this->model_module->user_id 			= $this->pSQL($user_id);
		$this->model_module->cedula 			= $this->pSQL($cedula);
		$this->model_module->tel_movil 			= $this->pSQL($tel_movil);
		$this->model_module->tel_habitacion 	= $this->pSQL($tel_habitacion);
		$this->model_module->tel_contacto 		= $this->pSQL($tel_contacto);
		$this->model_module->nivel_profesion 	= $this->pSQL($nivel_profesion);
		$this->model_module->area_profesional 	= $this->pSQL($area_profesional);
		$this->model_module->estado 			= $this->pSQL($estado);
		$this->model_module->ciudad 			= $this->pSQL($ciudad);
		$this->model_module->parroquia 			= $this->pSQL($parroquia);
		$this->model_module->direccion 			= $this->pSQL($direccion);
		$this->model_module->sede_id 			= $this->pSQL($sede_id);
		$this->model_module->status 			= $this->pSQL($status);
		
		$this->model_module->create();
		$id = $this->model_module->last_id();
		
		$this->model_module->commit();
		//Obtener el registro actual
		
		$this->model_module_view->id = $id;
		$data = $this->model_module_view->find();
		
		//Devolver la respuesta
		$this->view->response(201, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido guardado',
			'icon' => 'success',
			'data' => $data
		));
	}
	
	/**
	* 
	* 
	* @return
	*/
	public function put(){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		$id 		= $this->get_put('id');
		$name 		= $this->get_put('name');
		$last_name 	= $this->get_put('last_name');
		$email 		= $this->get_put('email');
		
		$cedula 	= $this->get_put('cedula');
		$tel_movil 	= $this->get_put('tel_movil');
		$tel_habitacion 	= $this->get_put('tel_habitacion');
		$tel_contacto 		= $this->get_put('tel_contacto');
		$nivel_profesion 	= $this->get_put('nivel_profesion');
		$area_profesional 	= $this->get_put('area_profesional');
		$estado 	= $this->get_put('estado');
		$ciudad 	= $this->get_put('ciudad');
		$parroquia 	= $this->get_put('parroquia');
		$direccion	= $this->get_put('direccion');
		$sede_id 	= $this->get_put('sede_id');
		$status 	= $this->get_put('status');
						
		//Validación de campos
		if( strlen($cedula) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'La <strong>Cédula</strong> es requerida para continuar.',
					'field' => 'codigo',
					'icon'=>'warning',
					'data' => $_POST
				)
			);
		} 
		
		if( strlen($name) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>Nombre</strong> es requerido para continuar. Debe tener entre 3 y 25 caracteres.',
					'field' => 'name',
					'data' => $_POST
				)
			);
		} 
		
		if( strlen($last_name) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>Apellido</strong> es requerido para continuar. Debe tener entre 3 y 25 caracteres.',
					'field' => 'last_name',
					'data' => $_POST
				)
			);
		} 
		
		if( $status < 0 OR $status > 1 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Seleccione el Estatus del registro para continuar.',
					'field' => 'status',
					'data' => $_POST
				)
			);
		} 
		
		if( $sede_id <0 ){
			$sede_id = NULL;
		}
		
		//Validar existencia previa del registro		
		
		//Validar existencia previa del registro
		$this->model_module_view->id = $id;
		$exists = $this->model_module_view->find();
		
		if( !$exists['user_id'] ){
			$this->view->response(422, 
				array(
					'statusText'=>'Existe un registro con el mismo <strong>E-mail</strong> ingresado.',
					'field' => 'email',
					'data' => $_POST
				)
			);
		}
		
		$user_id = $exists['user_id'];
		
		$this->model_module->begin();
		
		//Crear usuario en la tabla de usuario
		$this->model_users->profiles_id = NULL;
		$this->model_users->name 		= $this->pSQL($name);
		$this->model_users->last_name 	= $this->pSQL($last_name);
		$this->model_users->user_type 	= UserType::PROFESOR;
		$this->model_users->ambit	 	= BACKEND;
		
		$this->model_users->update();
		
		$area_profesional = implode(',',$area_profesional);
		
		//Asignación de valores y Guardado
		$this->model_module->user_id 			= $this->pSQL($user_id);
		$this->model_module->cedula 			= $this->pSQL($cedula);
		$this->model_module->tel_movil 			= $this->pSQL($tel_movil);
		$this->model_module->tel_habitacion 	= $this->pSQL($tel_habitacion);
		$this->model_module->tel_contacto 		= $this->pSQL($tel_contacto);
		$this->model_module->nivel_profesion 	= $this->pSQL($nivel_profesion);
		$this->model_module->area_profesional 	= $this->pSQL($area_profesional);
		$this->model_module->estado 			= $this->pSQL($estado);
		$this->model_module->ciudad 			= $this->pSQL($ciudad);
		$this->model_module->parroquia 			= $this->pSQL($parroquia);
		$this->model_module->direccion 			= $this->pSQL($direccion);
		$this->model_module->sede_id 			= $this->pSQL($sede_id);
		$this->model_module->status 			= $this->pSQL($status);
		
		$this->model_module->update( $id );
		
		//Obtener el ID asigando desde la Base de Datos
		$affected = $this->model_module->affected();
		
		$this->model_module->commit();
		//Obtener el registro actual
		
		$this->model_module_view->id = $id;
		$data = $this->model_module_view->find();
		
		//Devolver la respuesta
		$this->view->response(201, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido guardado',
			'icon' => 'success',
			'data' => $data
		));
		
	}
	
	/**
	* 
	* 
	* @return
	*/
	public function delete( $id = NULL ){
		
		if( strlen($id) < 1 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Se debe especificar un ID válido para continuar con la operación',
					'icon'=>'warning',
					'data' => array('id'=>$id)
				)
			);
		} 
		
		
		$this->model_module->delete($id);
		
		//Devolver la respuesta
		$this->view->response(204, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido eliminado',
			'icon' => 'success',
			'data' => array('id'=>$id)
		));
	}
	
}
?>