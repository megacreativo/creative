<?php

class sedesController extends Controller {
	
	private $_filters = array( 
		'Todos'		=> 'all',
		'Código'	=> 'codigo',
		'Nombre'	=> 'nombre',
	);
	
	
	function __construct() {		
		parent::__construct(__CLASS__);
		$this->controller = __CLASS__;		
		$this->module = str_ireplace('controller','', $this->controller);		
		$this->model_module = $this->load_model($this->module);
	}
	
	/**
	* Utilizado para el Metodo GET
	* 
	* @return
	*/
	public function index(){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		$data = $this->model_module->all( array(
			"status_text" =>
				"CASE 
					WHEN status = 0 THEN 'Inactiva' 
					WHEN status = 1 THEN 'Activa' 
				END",
			"status_class" =>
				"CASE 
					WHEN status = 0 THEN 'danger' 
					WHEN status = 1 THEN 'success' 
				END",
			"status_info" => 
				"CASE 
					WHEN status = 0 THEN 'Carrera inactiva' 
					WHEN status = 1 THEN 'Carrera activa' 
				END",
			)
		);
		$this->view->response(200, 
			array(
				'method'	=> 'GET',
				'data'		=> $data,
				'count' 	=> count($data)
			)
		);
	}
	
	
	/**
	* 
	* 
	* @return
	*/
	public function find( $id = NULL ){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		$this->model_module->id = $id;
		$data = $this->model_module->find( array(
			"status_text" =>
				"CASE 
					WHEN status = 0 THEN 'Inactiva' 
					WHEN status = 1 THEN 'Activa' 
				END",
			"status_class" =>
				"CASE 
					WHEN status = 0 THEN 'danger' 
					WHEN status = 1 THEN 'success' 
				END",
			"status_info" => 
				"CASE 
					WHEN status = 0 THEN 'Carrera inactiva' 
					WHEN status = 1 THEN 'Carrera activa' 
				END",
			)
		);
		$this->view->response(200, 
			array(
				'method'	=> 'GET',
				'data'		=> $data,
				'count' 	=> count($data)
			)
		);
	}
	


	/**
	* 
	* 
	* @return
	*/
	public function search( $filter, $value){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		if( !array_search( $filter, $this->_filters ) ){
			$this->view->response(422, 
				array(
					'method'	=> 'GET',
					'data'		=> array(),
					'count' 	=> 0
				)
			);
		}
		
		if( $filter == 'all' ){
			$values = array();
			
			foreach( $this->_filters as $key => $val){
				if( $val !== 'all' ){
					$filters[$val] = "%".$value."%";
				}
			}
			
			$data = $this->model_module->likeor(
				$filters,
				array(
					"status_text" =>
						"CASE 
							WHEN status = 0 THEN 'Inactiva' 
							WHEN status = 1 THEN 'Activa' 
						END",
					"status_class" =>
						"CASE 
							WHEN status = 0 THEN 'danger' 
							WHEN status = 1 THEN 'success' 
						END",
					"status_info" => 
						"CASE 
							WHEN status = 0 THEN 'Carrera inactiva' 
							WHEN status = 1 THEN 'Carrera activa' 
						END",
				)
			);
			
		} else {
			
			$data = $this->model_module->likeor( 
				array($filter=>$value), 
				array(
					"status_text" =>
						"CASE 
							WHEN status = 0 THEN 'Inactiva' 
							WHEN status = 1 THEN 'Activa' 
						END",
					"status_class" =>
						"CASE 
							WHEN status = 0 THEN 'danger' 
							WHEN status = 1 THEN 'success' 
						END",
					"status_info" => 
						"CASE 
							WHEN status = 0 THEN 'Carrera inactiva' 
							WHEN status = 1 THEN 'Carrera activa' 
						END",
				)
			);
		}
		
		
		
		$this->view->response(200, 
			array(
				'method'	=> 'GET',
				'data'		=> $data,
				'count' 	=> count($data)
			)
		);
	}
	
	
	
	/**
	* Guardar un nuevo registro
	* 
	* @return
	*/
	public function post( ){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		//Recoger valor de variables
		$nombre = $this->get_string('nombre');
		$estado = $this->get_string('estado');
		$ciudad = $this->get_string('ciudad');
		$municipio = $this->get_string('municipio');
		$parroquia = $this->get_string('parroquia');
		$direccion = $this->get_string('direccion');
		$status = $this->get_int('status');
		
		
		if( $nombre == '' ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>Nombre</strong> es requerido para continuar. Debe tener entre 3 y 10 caracteres.',
					'field' => 'nombre',
					'data' => $_POST
				)
			);
		} 
		
		$exists = $this->model_module->exists($nombre, 'nombre');
		if( $exists == TRUE ){
			$this->view->response(422, 
				array(
					'statusText'=>'Existe un registro con el misma <strong>Nombre</strong>.',
					'field' => 'nombre',
					'data' => $_POST
				)
			);
		}
		
		
		if( $status < 0 or $status > 1 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Seleccione el <strong>Estatus</strong> del registro para continuar.',
					'field' => 'status',
					'data' => $_POST
				)
			);
		} 
		
		$this->model_module->begin();
		
		
		//Asignación de valores y Guardado
		$this->model_module->nombre = $nombre;
		$this->model_module->estado = $estado;
		$this->model_module->ciudad = $ciudad;
		$this->model_module->municipio = $municipio;
		$this->model_module->parroquia = $parroquia;
		$this->model_module->direccion = $direccion;
		
		//Insertar Registro
		$this->model_users->create();
		
		//Finalizar transacción
		$this->model_module->commit();
		
		//Obtener el registro actual
		$this->model_module->id = $id;
		$data = $this->model_module->find( array(
			"status_text" =>
				"CASE 
					WHEN status = 0 THEN 'Inactiva' 
					WHEN status = 1 THEN 'Activa' 
				END",
			"status_class" =>
				"CASE 
					WHEN status = 0 THEN 'danger' 
					WHEN status = 1 THEN 'success' 
				END",
			"status_info" => 
				"CASE 
					WHEN status = 0 THEN 'Carrera inactiva' 
					WHEN status = 1 THEN 'Carrera activa' 
				END",
			)
		);
		
		//Devolver la respuesta
		$this->view->response(201, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido guardado',
			'icon' => 'success',
			'data' => $data
		));
	}
	
	/**
	* 
	* 
	* @return
	*/
	public function put(){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		//Recoger valor de variables
		$id		= $this->get_put('id');
		$nombre = $this->get_put('nombre');
		$estado = $this->get_put('estado');
		$ciudad = $this->get_put('ciudad');
		$municipio = $this->get_put('municipio');
		$parroquia = $this->get_put('parroquia');
		$direccion = $this->get_put('direccion');
		$status = $this->get_int('status');
		
		
		if( strlen($nombre) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>Nombre</strong> es requerido para continuar. Debe tener al menos 3 caracteres.',
					'field' => 'nombre',
					'data' => $this->get_all_put()
				)
			);
		} 
		
		if( $status < 0 or $status > 1 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Seleccione el Estatus del registro para continuar.',
					'field' => 'status',
					'data' => $this->get_all_put()
				)
			);
		} 
		
		$this->model_module->begin();
		
		
		//Asignación de valores y Guardado
		$this->model_module->nombre = $nombre;
		$this->model_module->estado = $estado;
		$this->model_module->ciudad = $ciudad;
		$this->model_module->municipio = $municipio;
		$this->model_module->parroquia = $parroquia;
		$this->model_module->direccion = $direccion;
		
		//Actualizar Registro
		$this->model_module->update( $id );		
		//Obtener el ID asigando desde la Base de Datos
		$affected = $this->model_module->affected();
		
		//Finalizar transacción
		$this->model_module->commit();
		
		
		//Obtener el registro actual
		$this->model_module->id = $id;
		$data = $this->model_module->find( array(
			"status_text" =>
				"CASE 
					WHEN status = 0 THEN 'Inactiva' 
					WHEN status = 1 THEN 'Activa' 
				END",
			"status_class" =>
				"CASE 
					WHEN status = 0 THEN 'danger' 
					WHEN status = 1 THEN 'success' 
				END",
			"status_info" => 
				"CASE 
					WHEN status = 0 THEN 'Carrera inactiva' 
					WHEN status = 1 THEN 'Carrera activa' 
				END",
			)
		);
		
		//Devolver la respuesta
		$this->view->response(201, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido guardado',
			'icon' => 'success',
			'affected'=>$affected,
			'data' => $data
		));
		
	}
	
	/**
	* 
	* 
	* @return
	*/
	public function delete( $id = NULL ){
		
		if( strlen($id) < 1 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Se debe especificar un ID válido para continuar con la operación',
					'icon'=>'warning',
					'data' => array('id'=>$id)
				)
			);
		} 
		
		
		$this->model_module->delete($id);
		
		//Devolver la respuesta
		$this->view->response(204, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido eliminado',
			'icon' => 'success',
			'data' => array('id'=>$id)
		));
	}
	
}
?>