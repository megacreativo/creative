<?php


class profilesController extends Controller {
	
	private $_filters = array( 
		'Todos'		=> 'all',
		'Nombre'	=> 'name',
		'Módulo de inicio'	=> 'default_module',
	);
	
	
	function __construct() {		
		parent::__construct(__CLASS__);
		$this->controller = __CLASS__;
		$this->module = str_ireplace('controller','', $this->controller);		
		$this->model_module = $this->load_model($this->module);
		$this->model_permissions = $this->load_model($this->module .'_permissions');
		$this->model_permissions->change_pk('profile_id');
	}
	
	/**
	* Utilizado para el Metodo GET
	* 
	* @return
	*/
	public function index(){		
		$this->basic_index(__FUNCTION__, func_get_args());
	}
	
	
	/**
	* 
	* 
	* @return
	*/
	public function find( $id = NULL ){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		$this->model_module->id = $id;
		$data = $this->model_module->find( array(
			"status_text" =>
				"CASE 
					WHEN status = 0 THEN 'Inactivo' 
					WHEN status = 1 THEN 'Activo' 
				END",
			"status_class" =>
				"CASE 
					WHEN status = 0 THEN 'danger' 
					WHEN status = 1 THEN 'success' 
				END",
			"status_info" => 
				"CASE 
					WHEN status = 0 THEN 'Inactivo' 
					WHEN status = 1 THEN 'Activo' 
				END",
			)
		);
		
		$this->model_permissions->profile_id = $id;
		$permissions = $this->model_permissions->search();
		$data['permissions'] = $permissions;
		
		
		$this->view->response(200, 
			array(
				'method'	=> 'GET',
				'count' 	=> count($data),
				'count_permissions' 	=> count($permissions),
				'data'		=> $data,
				
			)
		);
		
		
	}
	


	/**
	* 
	* 
	* @return
	*/
	public function search( $filter, $value){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		if( !array_search( $filter, $this->_filters ) ){
			$this->view->response(422, 
				array(
					'method'	=> 'GET',
					'data'		=> array(),
					'count' 	=> 0
				)
			);
		}
		
		if( $filter == 'all' ){
			$values = array();
			
			foreach( $this->_filters as $key => $val){
				if( $val !== 'all' ){
					$filters[$val] = "%".$value."%";
				}
			}
			
			$data = $this->model_module->likeor($filters, array(
				"status_text" =>
					"CASE 
						WHEN status = 0 THEN 'Inactivo' 
						WHEN status = 1 THEN 'Activo' 
					END",
				"status_class" =>
					"CASE 
						WHEN status = 0 THEN 'danger' 
						WHEN status = 1 THEN 'success' 
					END",
				"status_info" => 
					"CASE 
						WHEN status = 0 THEN 'Inactivo' 
						WHEN status = 1 THEN 'Activo' 
					END",
				)
			);
			
		} else {
			
			$data = $this->model_module->likeor( array($filter=>$value), array(
				"status_text" =>
					"CASE 
						WHEN status = 0 THEN 'Inactivo' 
						WHEN status = 1 THEN 'Activo' 
					END",
				"status_class" =>
					"CASE 
						WHEN status = 0 THEN 'danger' 
						WHEN status = 1 THEN 'success' 
					END",
				"status_info" => 
					"CASE 
						WHEN status = 0 THEN 'Inactivo' 
						WHEN status = 1 THEN 'Activo' 
					END",
				)
			);
		}
		
		
		
		$this->view->response(200, 
			array(
				'method'	=> 'GET',
				'data'		=> $data,
				'count' 	=> count($data)
			)
		);
	}
	
	
	
	/**
	* Guardar un nuevo registro
	* 
	* @return
	*/
	public function post( ){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		//Recoger valor de variables
		$name 			= $this->get_string('name');
		$description 	= $this->get_string('description');
		$default_module = $this->get_string('default_module');
		$status 		= $this->get_int('status');
		
		$permissions 	= $this->get_post('permissions');
		$access_field 	= $this->get_post('access_field');
		
		if( strlen($name) < 5 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>Nombre</strong> es requerido para continuar. Debe tener entre 5 y 25 caracteres.',
					'field' => 'name',
					'data' => $_POST
				)
			);
		} 
		
		if( strlen($default_module) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>Módulo por defecto</strong> es requerido para continuar.',
					'field' => 'default_module',
					'data' => $_POST
				)
			);
		} 
		
		if( $status < 0 OR $status > 1 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Seleccione el Estatus del registro para continuar.',
					'field' => 'status',
					'data' => $_POST
				)
			);
		} 
		
		
		$exists = $this->model_module->exists($name, 'name');
		if( $exists == TRUE ){
			$this->view->response(422, 
				array(
					'statusText'=>'Existe un registro con el mismo <strong>Nombre de Perfil</strong>.',
					'field' => 'name',
					'data' => $_POST
				)
			);
		}
		
		
		$this->model_module->begin();
		
		//Asignación de valores y Guardado
		$this->model_module->name 			= $this->pSQL($name);
		$this->model_module->description 	= $this->pSQL($description);
		$this->model_module->default_module	= $this->pSQL($default_module);
		$this->model_module->status	 		= $status;
				
		$this->model_module->create();
		$id = $this->model_module->last_id();
		
		$this->model_permissions->change_pk('profile_id');
		$this->model_permissions->delete($id);
		
		foreach( $permissions as $modulo => $modulo_attr ){
			$this->model_permissions->profile_id= $id;
			$this->model_permissions->name 		= $this->pSQL($modulo);
			$this->model_permissions->content	= implode(',',$modulo_attr);
			$this->model_permissions->attr		= 'permission-module';
					
			$this->model_permissions->create();
		}
		
		
		$this->model_module->commit();
		
		
		//Obtener el registro actual
		
		$this->model_module->id = $id;
		$data = $this->model_module->find( array(
			"status_text" =>
				"CASE 
					WHEN status = 0 THEN 'Inactiva' 
					WHEN status = 1 THEN 'Activa' 
				END",
			"status_class" =>
				"CASE 
					WHEN status = 0 THEN 'danger' 
					WHEN status = 1 THEN 'success' 
				END",
			"status_info" => 
				"CASE 
					WHEN status = 0 THEN 'Carrera inactiva' 
					WHEN status = 1 THEN 'Carrera activa' 
				END",
			)
		);
		
		$this->model_module->profile_id = $id;
		$permissions = $this->model_permissions->search();
		$data['permissions'] = $permissions;
		
		//Devolver la respuesta
		$this->view->response(201, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido guardado',
			'icon' => 'success',
			'data' => $data
		));
	}
	
	
	/**
	* 
	* 
	* @return
	*/
	public function put(){
		
		$this->run_method(__FUNCTION__, func_get_args());
		
		//Recoger valor de variables
		$id 			= $this->get_put('id');
		$name 			= $this->get_put('name');
		$description 	= $this->get_put('description');
		$default_module = $this->get_put('default_module');
		$status 		= $this->get_int('status');
		
		$permissions 	= $this->get_put('permissions');
		$access_field 	= $this->get_put('access_field');
		
		if( strlen($name) < 5 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>Nombre</strong> es requerido para continuar. Debe tener entre 5 y 25 caracteres.',
					'field' => 'name',
					'data' => $_POST
				)
			);
		} 
		
		if( strlen($default_module) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'El <strong>Módulo por defecto</strong> es requerido para continuar.',
					'field' => 'default_module',
					'data' => $_POST
				)
			);
		} 
		
		if( $status < 0 OR $status > 1 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Seleccione el Estatus del registro para continuar.',
					'field' => 'status',
					'data' => $_POST
				)
			);
		} 
		
		
		$this->model_module->begin();
		
		//Asignación de valores y Guardado
		$this->model_module->name 			= $this->pSQL($name);
		$this->model_module->description 	= $this->pSQL($description);
		$this->model_module->default_module	= $this->pSQL($default_module);
		$this->model_module->status	 		= $status;
				
		$this->model_module->update( $id );
		
		//Obtener el ID asigando desde la Base de Datos
		$affected = $this->model_module->affected();
		
		
		$this->model_permissions->change_pk('profile_id');
		$this->model_permissions->delete($id);
		
		foreach( $permissions as $modulo => $modulo_attr ){
			$this->model_permissions->profile_id= $id;
			$this->model_permissions->name 		= $this->pSQL($modulo);
			$this->model_permissions->content	= implode(',',$modulo_attr);
			$this->model_permissions->attr		= 'permission-module';
			$this->model_permissions->create();
		}
		
		foreach( $access_field as $modulo => $modulo_access ){
			$access = '';
			foreach( $modulo_access as $key => $value ){
				$access .= $key.':'.$value['access'].',';
			}
			$access = substr($access, 0, strlen($access)-1 );
			$this->model_permissions->profile_id= $id;
			$this->model_permissions->name 		= $this->pSQL($modulo);
			$this->model_permissions->content	= $access;
			$this->model_permissions->attr		= 'permission-field';
			$this->model_permissions->create();
		}
		
		$this->model_module->commit();
		
		
		//Obtener el registro actual
		
		$this->model_module->id = $id;
		$data = $this->model_module->find( array(
			"status_text" =>
				"CASE 
					WHEN status = 0 THEN 'Inactiva' 
					WHEN status = 1 THEN 'Activa' 
				END",
			"status_class" =>
				"CASE 
					WHEN status = 0 THEN 'danger' 
					WHEN status = 1 THEN 'success' 
				END",
			"status_info" => 
				"CASE 
					WHEN status = 0 THEN 'Carrera inactiva' 
					WHEN status = 1 THEN 'Carrera activa' 
				END",
			)
		);
		
		$this->model_module->profile_id = $id;
		$permissions = $this->model_permissions->search();
		$data['permissions'] = $permissions;
		
		
		//Devolver la respuesta
		$this->view->response(201, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido guardado',
			'icon' => 'success',
			'data' => $data
		));		
	
	}
	
	/**
	* 
	* 
	* @return
	*/
	public function delete( $id = NULL ){
		
		if( strlen($id) < 1 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Se debe especificar un ID válido para continuar con la operación',
					'icon'=>'warning',
					'data' => array('id'=>$id)
				)
			);
		} 
		
		
		$this->model_module->delete($id);
		
		//Devolver la respuesta
		$this->view->response(204, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido eliminado',
			'icon' => 'success',
			'data' => array('id'=>$id)
		));
	}
	
}
?>