<?php

if( !defined('CREATIVE') ) die( 'Can not access from here' );

require_once 'usertype.php';

class accountsController extends Controller {
	
	function __construct() {
		parent::__construct(__CLASS__);
		$this->controller = __CLASS__;
		$this->module = str_ireplace('controller','', $this->controller);
		$this->conf = Creative::load_config( 'auth' ) ;	
	}
	

	/**
	* Utilizado para el Metodo GET
	* 
	* @return
	*/
	public function index(){		
		$this->run_method(__FUNCTION__, func_get_args());
	}	
    
    
    /**
	* Iniciar sessión
	* 
	* @return
	*/
	public function auth(){
		
		$email 	= $this->get_string('email');
		$pass 	= $this->get_string('pass');
		$backend= $this->get_int('backend');

		#Validación de Email		
  		if( !$this->is_email($email) ){
			$this->view->response(422, 
				array(
					'statusText'=>'Debe ingresar un <strong>Email</strong> para continuar.',
					'field' => 'codigo',
					'icon'=>'warning',
					'data'	=> array('email'=>$email)
				)
			);
		}
		
  		#Validación de Contraseña  		
  		if( $pass == '' ){
			$this->view->response(422, 
				array(
					'statusText'=>'Debe ingresar una <strong>Contraseña</strong> para continuar.',
					'field' => 'codigo',
					'icon'=>'warning',
					'data'	=> array('email'=>$email)
				)
			);
		}
		
		if( $backend ){
			$model_name = 'administrators';
		} else {
			$model_name = 'users';
		}

		$this->model = $this->load_model($model_name);
		$this->model->email = $email;		
		$data = $this->model->search();
		
		if( is_array($data) AND count($data) ){
			
			$data = $data[0];
			
			if( $data == NULL ){
				$this->view->response(401, 
					array(
						'statusText'=>'La combinación de <strong>Email y contraseñana</strong> es válida.',
						'field' => '#email',
						'icon'	=>'warning',
						'data'	=> array('email'=>$email)
					)
				);
			}
			
			//Cuenta Bloqueada o Inactiva
			if( $data['status'] == $this->conf->user_status['inactive']){
				$this->view->response(401, 
					array(
						'statusText'=>'La cuenta ha sido bloqueada. contácte al <strong>Administrador de Sistema</strong> para obtener más información',
						'icon'	=>'warning',
						'data'	=> array('email'=>$email)
					)
				);
			}
			
			//Activar Cuenta
			if( $data['status'] == $this->conf->user_status['pending_activation'] ){
				$this->view->response(401, 
					array(
						'statusText'=>'Debe confirmar su registro para ingresar. Hemos enviado un Correo Electrónico con los pasos a seguir para activar su cuenta.',
						'field' => 'email',
						'icon'	=>'warning',
						'data'	=> array('email'=>$email)
					)
				);
			}
			
			//Obtener Contraseña y Hash
			$hash_db	= $data['hash'];
			$pass_db	= $data['pass'];
			
			//Encriptar Contraseña ingresada con el HASH de la Base de Datos
			$pass_enc 	= $this->encode( $pass, $hash_db);
			
			//si coincide la contraseña
			if( $pass_enc === $pass_db ){
				
				$this->model_profile = $this->load_model('profiles');
				$this->model_profile->id = $data['profile_id'];		
				$data_profile = $this->model_profile->find();
				
				
				if( $data_profile['default_module'] == 'dashboard' ){
					$default_module = 
						'/'
						. ($backend ? BACKEND : FRONTEND) 
						. '/?tokenurl='  
						. strtoupper(substr(sha1(time()), 0, 6));
				} else {
					$default_module = 
						'/' 
						. ($backend ? BACKEND : FRONTEND) 
					 	. '/'
						. $data_profile['default_module'] 
						. '/?tokenurl='
						. strtoupper(substr(sha1(time()), 0, 6));
				}
				
				
				$data_session = array(
	    			'auth'			=> TRUE,				//Indica si está autenticado
	    			'id'			=> $data['id'],
	    			'name'			=> $data['name'],
	    			'last_name'		=> $data['last_name'],
	    			'description'	=> $data['name'].' '.$data['last_name'],
	    			'email'			=> $data['email'],
	    			'profile_id'	=> $data['profile_id'],
	    			'profile_name'	=> $data_profile['name'],
	    			'default_module'=> $default_module,
	    			'permissions'	=> array(),
		    		'sesion_start'	=> time(),
		    		'sesion_time' 	=> time(),
	    		);
		    	
		    	
		    	
				//Sesión de usuario del sistema
				Session::set( $backend ? BACKEND : FRONTEND , $data_session);
				
				$this->view->response(200, 
					array(
						'icon'			=>'success',
						'statusText'	=> 'Bienvenido (a), por favor espere será redireccionado.',
						'default_module'=> $default_module,
						'data'			=> $data_session,
					)
				);
				
			} else {
				$this->response_json(
					array(
						'status'	 => 300,
						'response' => array(
							'message'	=> self::MSG_ERROR_DATOS,
							'icon'		=> 'error',
							'field'		=> 'email,password',
							'token'		=> generate_token(__CLASS__)
						)
					)
				);
			}
			
		} else {
			$this->view->response(401, 
				array(
					'statusText'=>'La combinación de <strong>Email y contraseña</strong> no es válida',
					'field' => '#email,#pass',
					'icon'	=>'error',
					'data'	=> array('email'=>$email)
				)
			);
				
		}
		
	}
	
	
	/**
	* Cerrar sessión
	* 
	* @return
	*/
	public function signout( ){
		$ambit = $this->get_int('ambit');
		Session::destroy($ambit == BACKEND ? BACKEND : FRONTEND);
	}
	
	//------------------------------------------------
	
	
	public function profile( $action ){
		switch( $action ){
			case 'update' :
				$this->profile_update();
			break;
			case 'security' :
				$this->profile_security();
			break;
		}
	}
	
	
	private function profile_update(){
		
		$this->validate_auth();
		
		$id			= Session::get( BACKEND )['id'];
		
		$dni 		= $this->get_string('dni');
		$name 		= $this->get_string('name');
		$last_name 	= $this->get_string('last_name');
		$last_name 	= $this->get_string('last_name');
		$image 		= $this->get_file('image');
		
		
		#Validación de Email		
  		if( strlen($dni) < 6 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Debe ingresar una <strong>Cédula de Identidad</strong> para continuar.',
					'field' => 'dni',
					'icon'=>'warning',
					'data'	=> $_POST
				)
			);
		}
		
		#Nombre
		if( strlen($name) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Debe ingresar su <strong>Nombre</strong> para continuar.',
					'field' => 'name',
					'icon'	=>'warning',
					'data'	=> $_POST
				)
			);
		}
		
		#Apellido
		if( strlen($last_name) < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=>'Debe ingresar su <strong>Apellido</strong> para continuar.',
					'field' => 'last_name',
					'icon'	=>'warning',
					'data'	=> $_POST
				)
			);
		}
		
		
		$this->model_module = $this->load_model('users');
		
		$this->model_module->begin();
		
		$this->model_module->dni 			= $this->pSQL($dni);
		$this->model_module->name			= $this->pSQL($name);
		$this->model_module->last_name		= $this->pSQL($last_name);
		
		
		$image_name = strtolower( basename($image['name']));
		$image_ext	= pathinfo($image_name, PATHINFO_EXTENSION);		
		$image_type_allow =  array('gif','png' ,'jpg');
		
		if( !in_array($image_ext, $image_type_allow) ) {
			//Devolver la respuesta
			$this->view->response(422, array(
				'statusText'=>'Formato de imagen inválido',
				'icon' => 'error',
				'data' => $_POST
			));
		}

		if( $image_name != '' ){
			$image_name		= strtolower($dni.'-'.$name.'-'.$last_name.'.'.$image_ext);
			$image_type 	= $image['type'];
			$image_error 	= $image['error'];
			$image_content 	= $image['tmp_name'];
			
			$path = ROOT .'../content/uploads/users/'. $image_name;
			
			if( !move_uploaded_file( $image_content, $path ) ){
				$this->model_module->rollback();
				$this->view->response(500, array(
					'statusText'=>'No se pudo subir la imagen',
					'icon' => 'error',
					'data' => $_POST
				));
			} else {
				$this->model_module->image = '/content/uploads/users/'. $image_name;
			}
		}
		
		
		$this->model_module->update( $id );
		$this->model_module->commit();
		
		$this->model_module->id = $id;
		$data = $this->model_module->find();
		
		//Devolver la respuesta
		$this->view->response(201, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido guardado',
			'icon' => 'success',
			'data' => $data,
		));
		
	}
	
	
	/**
	* 
	* 
	* @return
	*/
	private function profile_security(){
		
		$this->validate_auth();
		
		$id			= Session::get( BACKEND )['id'];
		$pass1		= $this->get_string('pass1');
		$pass2 		= $this->get_string('pass2');
		
		$check = $this->check_pass($pass1);
		#Nombre
		if( $check['status'] < 3 ){
			$this->view->response(422, 
				array(
					'statusText'=> $check['statusText'],
					'field' => '#pass1,#pass2',
					'icon'	=> 'warning',
				)
			);
		}
		
		#Validación de Email		
  		if( $pass1 != $pass2 ){
			$this->view->response(422, 
				array(
					'statusText'=> 'Las <strong>Contraseñas</strong> ingresadaas no coinciden',
					'field' => '#pass1,#pass2',
					'icon'	=> 'warning',
				)
			);
		}
		
		$pass = $this->encode($pass1, HASH_KEY);
		
		$this->model_module = $this->load_model('users');		
		$this->model_module->begin();
		$this->model_module->pass 	= $pass;
		$this->model_module->hash	= HASH_KEY;
		$this->model_module->update( $id );
		$this->model_module->commit();
		
		$this->view->response(201, array(
			'statusText'=>'<strong>¡Éxito!</strong><br/>El registro ha sido guardado',
			'icon' => 'success',
		));
		
	}
	
	//------------------------------------------------
	
	public function reset_password(){
		
	}
	
	
	public function post(  ){
		$this->run_method(__FUNCTION__, func_get_args());
	}
	

}
?>