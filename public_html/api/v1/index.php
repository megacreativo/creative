<?php

	$response = array(
		'status'=>200, 
		'statusText'=>'Ok', 
		'version'=>'1',
		'time'=>time()
	);			
	http_response_code(200);
	header('Content-Type: application/json; charset=utf8');
	echo json_encode($response, JSON_PRETTY_PRINT);
	exit;
?>