<?php

/**
 * Función de autoload
 * Se utiliza para incluir el controlador apropiado, solo cuando se necesita
 * @param String el nombre de la clase
 */ 
function autoload_app($ClassName) {
	if(file_exists( __DIR__ .DS. '/kernel' .DS. $ClassName . '.php')){
		include_once( __DIR__ .DS. '/kernel' .DS. $ClassName . '.php' );
	}
}
spl_autoload_register('autoload_app');



function autoload_controllers($ClassName) {
	/*if(file_exists(PATH_CONTROLLERS . $ClassName . '.php')){
		include_once(PATH_CONTROLLERS . $ClassName . '.php' );
	}*/
}
spl_autoload_register('autoload_controllers');
