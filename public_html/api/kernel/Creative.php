<?php

require_once 'CreativeBase.php';
require_once 'Initialize.php';


/**
 * Registro de Clases Creativo
 * Implementa el registro y el patrón de diseño Singleton
 *
 * @version 4.0.0
 * @author Brayan E. Rincoon
 */
class  Creative extends CreativeBase {
	/**
	 * Colección de objetos
	 * @access private
	 */
	private static $objects = array();
	
	
	/**
	 * Colección de ajustes y configuraciones
	 * @access private
	 */
	private static $settings = array();
	
	
	/**
	 * La instancia del registro
	 * @access private
	 */
	private static $instance;
	
	
	/**
	* Constructor privado para evitar que se creen directamente
	* @access private
	*/
	private function __construct(){ }		
	
	
	/**
	 * Asegura que solo se pueda crear una instancia de la clase y proporcionar un punto global de acceso a ella.
	 * @access public
	 * @return 
	 */
	public static function singleton(){
		if( !isset( self::$instance ) or !self::$instance instanceof self){
			$mybase = __CLASS__;
			self::$instance = new $mybase;
		}			
		return self::$instance;
	}					
	

	
	/**
	* Almacena un objeto en el registro
	* @param String $class El nombre del objeto
	* @param String $key El índice que sirve como identificador en el array	*
	* @return void
	*/
	public static function add( $class ){
		self::$objects[$class] = new $class( self::$instance );	
		return self::get($class);
	}
	
	
	/**
	 * Obtiene un objeto del registro
	 * @param String $key El índice del array
	 * @return object
	 */
	public static function get( $key ){
		if( is_object(self::$objects[$key]) ){
			return self::$objects[$key];
		}
	}
	
	
	
	//------------------------------------------------------------------
	
	/**
	 * Almacena los ajustes en el registro
	 * @param String $data
	 * @param String $key El índice del array
	 * @return void
	 */
	public function set_setting( $key, $data ){
		self::$settings[$key] = $data;
	}
	
	/**
	 * Obtiene un ajuste del registro
	 * @param String $key El índice del array
	 * @return void
	 */
	public function get_setting( $Key ){
		return self::$settings[$Key];
	}
	
	
	function create_core(){
		$this->add("DataBase");
	}
		
	
	//------------------------------------------------------------------
	
	/**
	* Evita la clonación del objeto
	* 
	* @return
	*/
	public function __clone(){
		trigger_error( 'Cloning of this object is not allowed', E_USER_ERROR );
	}
	
	/**
	* 
	* 
	* @return
	*/
	public function __wakeup(){
		trigger_error("No puedes deserializar una instancia de ". get_class($this) ." class.");
	}
   
    public function __sleep(){
        throw new Exception('Class  '.__CLASS__ . 'cannot be serialized');
    }
 

}

?>