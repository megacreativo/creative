<?php 

return (object) [

    'frontend',

    'backend',

    'user_status' => array(
        'logical_erasure' => -1,
        'inactive' => 0,
        'active' => 1,
        'pending_activation' => 2,
    ),

    'user_type' => array(
        'guest',
        'admin'
    )

];

?>